﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BatchMailer
{
    public partial class Default : Page
    {
        private DetailsController detialsController = new DetailsController();
        private List<DetailsInfo> detailsInfoList = new List<DetailsInfo>();
        private DetailsInfo detailsInfo = new DetailsInfo();
        private int detailsId = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "pageLoad", "javascript:loadTable(); ", true);
            }
            DataLoad();
        }

        public void DataLoad()
        {
            lblMessage.Text = string.Empty;
            try
            {
                detailsInfoList = detialsController.GetAllDetails();
                gv.DataSource = detailsInfoList;
                gv.DataBind();
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
            }
        }

        protected void gv_PreRender(object sender, EventArgs e)
        {
            GridView gv = (GridView)sender;

            if ((gv.ShowHeader == true && gv.Rows.Count > 0)
                || (gv.ShowHeaderWhenEmpty == true))
            {
                gv.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
            if (gv.ShowFooter == true && gv.Rows.Count > 0)
            {
                gv.FooterRow.TableSection = TableRowSection.TableFooter;
            }
        }

        protected void gv_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            GridViewRow gvr = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
            int index = gvr.RowIndex;
            int detailsId = -1;
            DetailsInfo detailsInfoMatch = null;
            if (index != -1)
            {
                // Get the detailsId and object class
                detailsId = Convert.ToInt32(gv.DataKeys[index].Value.ToString());
                detailsInfoMatch = detailsInfoList.Where(i => i.DetailsId == detailsId).First();
            }

            if (e.CommandName.Equals("add"))
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "AddShowModalScript", "$('#addModal').modal('show');", true);
            }
            if (e.CommandName.Equals("details"))
            {
                var detailInfos = detailsInfoList.Where(i => i.DetailsId == detailsId);
                detailsView.DataSource = detailInfos;
                detailsView.DataBind();
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "DetailModalScript", "$('#detailsModal').modal('show');", true);
            }
        }

        protected void gv_RowEditing(object sender, GridViewEditEventArgs e)
        {
            int detailsId = Convert.ToInt32(gv.DataKeys[e.NewEditIndex].Value.ToString());
            DetailsInfo detailsInfoMatch = detailsInfoMatch = detailsInfoList.Where(i => i.DetailsId == detailsId).First();
            hiddenIDUpdate.Value = detailsInfoMatch.DetailsId.ToString();
            txtFirstNameUpdate.Text = detailsInfoMatch.FirstName;
            txtLastNameUpdate.Text = detailsInfoMatch.LastName;
            txtAddressUpdate.Text = detailsInfoMatch.Address;
            txtCityUpdate.Text = detailsInfoMatch.City;
            txtRegionUpdate.Text = detailsInfoMatch.Region;
            txtPostalCodeUpdate.Text = detailsInfoMatch.PostalCode;
            txtCountryUpdate.Text = detailsInfoMatch.Country;
            txtPhoneUpdate.Text = detailsInfoMatch.Phone;
            txtEmailAddressUpdate.Text = detailsInfoMatch.EmailAddress;
            lblResult.Visible = false;
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "EditModalScript", "$('#editModal').modal('show');", true);
        }

        protected void gv_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            int detailsId = Convert.ToInt32(gv.DataKeys[e.RowIndex].Value.ToString());
            DetailsInfo detailsInfoMatch = detailsInfoMatch = detailsInfoList.Where(i => i.DetailsId == detailsId).First();
            HfDeleteID.Value = detailsInfoMatch.DetailsId.ToString();
            lblFirstNameDelete.Text = detailsInfoMatch.FirstName;
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "DeleteModalScript", "$('#deleteModal').modal('show');", true);
        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {
            if (IsPostBack)
            {
                if (!fileUpload.PostedFile.FileName.Equals(""))
                {
                    string fileExtension = Path.GetExtension(fileUpload.PostedFile.FileName);

                    if (fileExtension.ToLower() != ".xls" && fileExtension.ToLower() != ".xlsx")
                    {
                        uploadMessage.Text = "Only excel files are allowed(.xls, .xlsx)";
                        uploadMessage.ForeColor = System.Drawing.Color.Red;
                    }

                    else
                    {
                        fileUpload.PostedFile.SaveAs(Server.MapPath("~/Uploads/" + fileUpload.PostedFile.FileName));
                        uploadMessage.Text = "File Uploaded successfully";
                        uploadMessage.ForeColor = System.Drawing.Color.Green;
                        Response.Redirect(Request.Url.AbsoluteUri);
                    }
                }
                else
                {
                    uploadMessage.Text = "File not uploaded";
                    uploadMessage.ForeColor = System.Drawing.Color.Red;
                }
            }
            ScriptManager.RegisterStartupScript(this, GetType(), "pageLoad", "javascript:loadTable(); ", true);
            ClientScript.RegisterStartupScript(this.GetType(), "UploadModalScript", "$('#uploadModal').modal('show');", true);
        }

        protected void uploadClose_Click(object sender, EventArgs e)
        {
            Response.Redirect(Request.Url.AbsoluteUri);
        }
        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                lblMessage.Text = string.Empty;
                try
                {
                    detailsInfo = new DetailsInfo();
                    detailsInfo.DetailsId = Convert.ToInt32(hiddenIDUpdate.Value);
                    detailsInfo.FirstName = txtFirstNameUpdate.Text;
                    detailsInfo.LastName = txtLastNameUpdate.Text;
                    detailsInfo.Address = txtAddressUpdate.Text;
                    detailsInfo.City = txtCityUpdate.Text;
                    detailsInfo.Region = txtRegionUpdate.Text;
                    detailsInfo.PostalCode = txtPostalCodeUpdate.Text;
                    detailsInfo.Country = txtCountryUpdate.Text;
                    detailsInfo.Phone = txtPhoneUpdate.Text;
                    detailsInfo.EmailAddress = txtEmailAddressUpdate.Text;
                    detialsController.UpdateDetails(detailsInfo);
                }
                catch (Exception ex)
                {
                    lblMessage.Text = ex.Message;
                }

                gv.EditIndex = -1;
                DataLoad();
                StringBuilder sb = new StringBuilder();
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "EditHideModalScript", "$('#editModal').modal('hide');", true);
                gridPanel.Update();
                ScriptManager.RegisterStartupScript(this, GetType(), "pageLoad", "javascript:loadTable(); ", true);
            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            lblMessage.Text = string.Empty;
            try
            {
                detailsId = Convert.ToInt32(HfDeleteID.Value);
                detialsController.DeleteDetails(detailsId);
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
            }

            DataLoad();
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "delHideModalScript", "$('#deleteModal').modal('hide');", true);
            gridPanel.Update();
            ScriptManager.RegisterStartupScript(this, GetType(), "pageLoad", "javascript:loadTable(); ", true);
        }

        protected void btnAddRecord_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                lblMessage.Text = string.Empty;
                try
                {
                    DetailsController objCtrl = new DetailsController();
                    DetailsInfo objInfo = new DetailsInfo();
                    objInfo.FirstName = txtFirstNameAdd.Text;
                    objInfo.LastName = txtLastNameAdd.Text;
                    objInfo.Address = txtAddressAdd.Text;
                    objInfo.City = txtCityAdd.Text;
                    objInfo.Region = txtRegionAdd.Text;
                    objInfo.PostalCode = txtPostalCodeAdd.Text;
                    objInfo.Country = txtCountryAdd.Text;
                    objInfo.Phone = txtPhoneAdd.Text;
                    objInfo.EmailAddress = txtEmailAddressAdd.Text;
                    objCtrl.AddDetails(objInfo);
                }
                catch (Exception ex)
                {
                    lblMessage.Text = ex.Message;
                }
                DataLoad();
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "AddHideModalScript", "$('#addModal').modal('hide');", true);
                gridPanel.Update();
                ScriptManager.RegisterStartupScript(this, GetType(), "pageLoad", "javascript:loadTable(); ", true);
            }
        }
    }
}