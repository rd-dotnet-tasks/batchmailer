﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;
using System.Web.UI;

namespace BatchMailer
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/bundles/css").Include(
                "~/lib/bootstrap/css/bootstrap.css",
                "~/Content/font-awesome.css"
            ).Include("~/lib/font-awesome/css/font-awesome.css", new CssRewriteUrlTransform())
            .Include("~/Styles/Master.css"));

            bundles.Add(new ScriptBundle("~/bundles/js").Include(
                "~/lib/jquery/jquery.js",
                "~/lib/popper.js/umd/popper.js",
                "~/lib/bootstrap/js/bootstrap.js"
            ));

            bundles.Add(new StyleBundle("~/bundles/datatables-css").Include(
                "~/lib/datatables/bs4/css/dataTables.bootstrap4.css",
                "~/lib/datatables/autofill/css/autoFill.bootstrap4.css",
                "~/lib/datatables/buttons/css/buttons.bootstrap4.css",
                "~/lib/datatables/colreorder/css/colReorder.bootstrap4.css",
                "~/lib/datatables/fixedcolumns/css/fixedColumns.bootstrap4.css",
                "~/lib/datatables/fixedheader/css/fixedHeader.bootstrap4.css",
                "~/lib/datatables/keytable/css/keyTable.bootstrap4.css",
                "~/lib/datatables/responsive/css/responsive.bootstrap4.css",
                "~/lib/datatables/rowgroup/css/rowGroup.bootstrap4.css",
                "~/lib/datatables/rowreorder/css/rowReorder.bootstrap4.css",
                "~/lib/datatables/scroller/css/scroller.bootstrap4.css",
                "~/lib/datatables/select/css/select.bootstrap4.css"
            ));

            bundles.Add(new ScriptBundle("~/bundles/datatables-js").Include(
                "~/lib/datatables/jszip/jszip.js",
                "~/lib/datatables/pdfmake/pdfmake.js",
                "~/lib/datatables/pdfmake/vfs_fonts.js",
                "~/lib/datatables/js/jquery.dataTables.js",
                "~/lib/datatables/bs4/js/dataTables.bootstrap4.js",
                "~/lib/datatables/autofill/js/dataTables.autoFill.js",
                "~/lib/datatables/autofill/js/autoFill.bootstrap4.js",
                "~/lib/datatables/buttons/js/dataTables.buttons.js",
                "~/lib/datatables/buttons/js/buttons.bootstrap4.js",
                "~/lib/datatables/buttons/js/buttons.colVis.js",
                "~/lib/datatables/buttons/js/buttons.html5.js",
                "~/lib/datatables/buttons/js/buttons.print.js",
                "~/lib/datatables/colreorder/js/dataTables.colReorder.js",
                "~/lib/datatables/colreorder/js/colReorder.bootstrap4.js",
                "~/lib/datatables/fixedcolumns/js/dataTables.fixedColumns.js",
                "~/lib/datatables/fixedcolumns/js/fixedColumns.bootstrap4.js",
                "~/lib/datatables/fixedheader/js/dataTables.fixedHeader.js",
                "~/lib/datatables/fixedheader/js/fixedHeader.bootstrap4.js",
                "~/lib/datatables/keytable/js/dataTables.keyTable.js",
                "~/lib/datatables/keytable/js/keyTable.bootstrap4.js",
                "~/lib/datatables/responsive/js/dataTables.responsive.js",
                "~/lib/datatables/responsive/js/responsive.bootstrap4.js",
                "~/lib/datatables/rowgroup/js/dataTables.rowGroup.js",
                "~/lib/datatables/rowgroup/js/rowGroup.bootstrap4.js",
                "~/lib/datatables/rowreorder/js/dataTables.rowReorder.js",
                "~/lib/datatables/rowreorder/js/rowReorder.bootstrap4.js",
                "~/lib/datatables/scroller/js/dataTables.scroller.js",
                "~/lib/datatables/scroller/js/scroller.bootstrap4.js",
                "~/lib/datatables/select/js/dataTables.select.js",
                "~/lib/datatables/select/js/select.bootstrap4.js"
            ));

            BundleTable.EnableOptimizations = true;
        }
    }
}