﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Configuration;

namespace BatchMailer
{
    public abstract class DataProvider
    {
        private static string ConnectionString = ConfigurationManager.ConnectionStrings["batchMailer"].ConnectionString;

        public static List<DetailsInfo> GetAllDetails()
        {
            DataTable dataTable = new DataTable();
            try
            {
                using (SqlConnection sqlConnection = new SqlConnection(ConnectionString))
                {
                    using (SqlCommand sqlCommand = new SqlCommand("GetDetails", sqlConnection))
                    {
                        sqlCommand.CommandType = CommandType.StoredProcedure;
                        sqlConnection.Open();
                        using (SqlDataReader dataReader = sqlCommand.ExecuteReader())
                        {
                            dataTable.Load(dataReader);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return Helper.DataTableToList<DetailsInfo>(dataTable);
        }

        public static DetailsInfo GetDetails(int detailsId)
        {
            DataTable dataTable = new DataTable();
            try
            {
                using (SqlConnection sqlConnection = new SqlConnection(ConnectionString))
                {
                    using (SqlCommand sqlCommand = new SqlCommand("GetDetails", sqlConnection))
                    {
                        sqlCommand.CommandType = CommandType.StoredProcedure;
                        sqlCommand.Parameters.Add(new SqlParameter("@DetailsId", detailsId));
                        sqlConnection.Open();
                        using (SqlDataReader dataReader = sqlCommand.ExecuteReader())
                        {
                            dataTable.Load(dataReader);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return Helper.DataTableToList<DetailsInfo>(dataTable.DataSet.Tables[0])[0];
        }

        public static void DeleteDetails(int detailsId)
        {
            DataTable dataTable = new DataTable();
            try
            {
                using (SqlConnection sqlConnection = new SqlConnection(ConnectionString))
                {
                    using (SqlCommand sqlCommand = new SqlCommand("DeleteDetails", sqlConnection))
                    {
                        sqlCommand.CommandType = CommandType.StoredProcedure;
                        sqlCommand.Parameters.Add(new SqlParameter("@DetailsId", detailsId));
                        sqlConnection.Open();
                        sqlCommand.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public static int AddDetails(DetailsInfo detailsInfo)
        {
            int detailsId = 0;
            try
            {
                using (SqlConnection sqlConnection = new SqlConnection(ConnectionString))
                {
                    using (SqlCommand sqlCommand = new SqlCommand("AddDetails", sqlConnection))
                    {
                        sqlCommand.CommandType = CommandType.StoredProcedure;
                        sqlCommand.Parameters.Add(new SqlParameter("@FirstName", detailsInfo.FirstName));
                        sqlCommand.Parameters.Add(new SqlParameter("@LastName", detailsInfo.LastName));
                        sqlCommand.Parameters.Add(new SqlParameter("@Address", detailsInfo.Address));
                        sqlCommand.Parameters.Add(new SqlParameter("@City", detailsInfo.City));
                        sqlCommand.Parameters.Add(new SqlParameter("@Region", detailsInfo.Region));
                        sqlCommand.Parameters.Add(new SqlParameter("@PostalCode", detailsInfo.PostalCode));
                        sqlCommand.Parameters.Add(new SqlParameter("@Country", detailsInfo.Country));
                        sqlCommand.Parameters.Add(new SqlParameter("@Phone", detailsInfo.Phone));
                        sqlCommand.Parameters.Add(new SqlParameter("@EmailAddress", detailsInfo.EmailAddress));
                        sqlConnection.Open();
                        detailsId = Convert.ToInt32(sqlCommand.ExecuteScalar());
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return detailsId;
        }

        public static void UpdateDetails(DetailsInfo detailsInfo)
        {
            try
            {
                using (SqlConnection sqlConnection = new SqlConnection(ConnectionString))
                {
                    using (SqlCommand sqlCommand = new SqlCommand("UpdateDetails", sqlConnection))
                    {
                        sqlCommand.CommandType = CommandType.StoredProcedure;
                        sqlCommand.Parameters.Add(new SqlParameter("@DetailsId", detailsInfo.DetailsId));
                        sqlCommand.Parameters.Add(new SqlParameter("@FirstName", detailsInfo.FirstName));
                        sqlCommand.Parameters.Add(new SqlParameter("@LastName", detailsInfo.LastName));
                        sqlCommand.Parameters.Add(new SqlParameter("@Address", detailsInfo.Address));
                        sqlCommand.Parameters.Add(new SqlParameter("@City", detailsInfo.City));
                        sqlCommand.Parameters.Add(new SqlParameter("@Region", detailsInfo.Region));
                        sqlCommand.Parameters.Add(new SqlParameter("@PostalCode", detailsInfo.PostalCode));
                        sqlCommand.Parameters.Add(new SqlParameter("@Country", detailsInfo.Country));
                        sqlCommand.Parameters.Add(new SqlParameter("@Phone", detailsInfo.Phone));
                        sqlCommand.Parameters.Add(new SqlParameter("@EmailAddress", detailsInfo.EmailAddress));
                        sqlConnection.Open();
                        sqlCommand.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}