﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Web;

namespace BatchMailer
{
    public class DetailsController
    {
        public List<DetailsInfo> GetAllDetails()
        {
            return DataProvider.GetAllDetails();
        }

        public DetailsInfo GetDetails(int detailsId)
        {
            return DataProvider.GetDetails(detailsId);
        }

        public int AddDetails(DetailsInfo detailsInfo)
        {
            return DataProvider.AddDetails(detailsInfo);
        }

        public void UpdateDetails(DetailsInfo detailsInfo)
        {
            DataProvider.UpdateDetails(detailsInfo);
        }

        public void DeleteDetails(int detailsId)
        {
            DataProvider.DeleteDetails(detailsId);
        }
    }
}