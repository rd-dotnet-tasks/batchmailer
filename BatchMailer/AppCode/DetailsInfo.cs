﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BatchMailer
{
    public class DetailsInfo
    {
        public int DetailsId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string Region { get; set; }
        public string PostalCode { get; set; }
        public string Country { get; set; }
        public string Phone { get; set; }
        public string EmailAddress { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }

        public string FullName
        {
            get
            {
                return FirstName + " " + LastName;
            }
        }

        public string FullAddress
        {
            get
            {
                return Address + ", " + City + ", " + Region + " " + PostalCode + " - " + Country;
            }
        }

        public string DisplayAddress
        {
            get
            {
                return Address + "<br />" + City + ", " + Region + " " + PostalCode + "<br />" + Country;
            }
        }
    }
}