﻿<%@ Page Title="Home Page" AutoEventWireup="true" MasterPageFile="~/Site.Master"
    CodeBehind="Default.aspx.cs" Inherits="BatchMailer.Default" %>

<asp:Content ID="HeadContent" ContentPlaceHolderID="head" runat="server">
    <%: System.Web.Optimization.Styles.Render("~/bundles/datatables-css") %>
    <%: System.Web.Optimization.Scripts.Render("~/bundles/datatables-js") %>
    <link href="Styles/Default.css" rel="stylesheet" />
</asp:Content>

<asp:Content ID="BodyContent" ContentPlaceHolderID="body" runat="server">

    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true"></asp:ScriptManager>

    <div id="uploadModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="uploadModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 id="uploadModalLabel">Upload</h3>
                </div>
                <div class="modal-body">
                    <div class="input-group">
                        <div class="custom-file">
                            <asp:FileUpload runat="server" CssClass="custom-file-input" ID="fileUpload" />
                            <label class="custom-file-label" for="fileUpload">Choose file</label>
                        </div>
                    </div>
                    <asp:Label ID="uploadMessage" runat="server"></asp:Label>
                </div>
                <div class="modal-footer">
                    <asp:Button Text="Submit" CssClass="btn btn-primary btn-sm" ID="uploadSubmit" OnClick="btnUpload_Click" runat="server" />
                    <asp:Button Text="Close" CssClass="btn btn-danger btn-sm" ID="uploadClose" OnClick="uploadClose_Click" runat="server" />
                </div>
            </div>
        </div>
    </div>

    <!-- Main grid -->
    <asp:UpdatePanel ID="gridPanel" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
        <ContentTemplate>
            <asp:GridView ID="gv" runat="server" AutoGenerateColumns="false" DataKeyNames="DetailsId"
                OnPreRender="gv_PreRender" OnRowEditing="gv_RowEditing" OnRowCommand="gv_RowCommand"
                OnRowDeleting="gv_RowDeleting"
                CssClass="table table-striped table-hover table-sm">
                <EmptyDataTemplate>
                    <asp:Image ID="noData" ImageUrl="~/Images/NoData.jpg" AlternateText="No Image"
                        runat="server" Visible="false" />No Data Found.
                </EmptyDataTemplate>
                <Columns>
                    <asp:BoundField DataField="FullName" SortExpression="FullName" HeaderText="Name" />
                    <asp:BoundField DataField="FullAddress" SortExpression="FullAddress" HeaderText="Address" />
                    <asp:BoundField DataField="Phone" SortExpression="Phone" HeaderText="Phone" />
                    <asp:BoundField DataField="EmailAddress" SortExpression="EmailAddress" HeaderText="Email Address" />
                    <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                        <HeaderTemplate>
                            <asp:LinkButton ID="btnAdd" runat="server" CommandName="add" CssClass="btn btn-sm btn-block"
                                Text='<i class="fa fa-plus mr-1"></i>  <span class="font-weight-bold">Add record</span>' />
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:LinkButton ID="btnDetails" runat="server" CommandName="details" Text='<i class="fa fa-info fa-fw" aria-hidden="true"></i>'
                                CssClass="btn btn-info btn-sm mr-1" />
                            <asp:LinkButton ID="btnEdit" runat="server" CommandName="edit" Text='<i class="fa fa-pencil fa-fw" aria-hidden="true"></i>'
                                CssClass="btn btn-warning btn-sm mr-1" />
                            <asp:LinkButton ID="btnDelete" runat="server" CommandName="delete" Text='<i class="fa fa-trash fa-fw" aria-hidden="true"></i>'
                                CssClass="btn btn-danger btn-sm mr-1" />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </ContentTemplate>
    </asp:UpdatePanel>

    <!-- Details -->
    <div id="detailsModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="detailsModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 id="detailsModalLabel">Details</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <asp:UpdatePanel ID="detailsPanel" runat="server">
                        <ContentTemplate>
                            <asp:DetailsView ID="detailsView" runat="server"
                                CssClass="table table-bordered table-hover table-sm" BackColor="White"
                                ForeColor="Black" FieldHeaderStyle-Wrap="false"
                                FieldHeaderStyle-Font-Bold="true" FieldHeaderStyle-BackColor="LavenderBlush"
                                FieldHeaderStyle-ForeColor="Black" BorderStyle="Groove"
                                AutoGenerateRows="False">
                                <Fields>
                                    <asp:BoundField DataField="FullName" HeaderText="Name" />
                                    <asp:BoundField DataField="DisplayAddress" HeaderText="Address"
                                        HtmlEncode="false" />
                                    <asp:BoundField DataField="Phone" HeaderText="Phone" />
                                    <asp:BoundField DataField="EmailAddress" HeaderText="EmailAddress" />
                                    <asp:BoundField DataField="CreatedDate" HeaderText="Created Date" />
                                    <asp:TemplateField HeaderText="Updated Date">
                                        <ItemTemplate>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Fields>
                            </asp:DetailsView>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="gv" EventName="RowCommand" />
                        </Triggers>
                    </asp:UpdatePanel>
                    <div class="modal-footer">
                        <button class="btn btn-danger btn-sm" data-dismiss="modal"
                            aria-hidden="true">
                            Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Edit -->
    <div id="editModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="editModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 id="editModalLabel">Edit Record</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <asp:UpdatePanel ID="editPanel" runat="server">
                    <ContentTemplate>
                        <div class="modal-body">
                            <asp:HiddenField ID="hiddenIDUpdate" runat="server" />
                            <table class="table table-sm">
                                <tr>
                                    <td>First Name :</td>
                                    <td>
                                        <asp:TextBox ID="txtFirstNameUpdate" runat="server" Width="100%" />
                                        <asp:RequiredFieldValidator ID="valFirstNameUpdate"
                                            ControlToValidate="txtFirstNameUpdate" EnableClientScript="False"
                                            Display="Dynamic" Text="<br />* First Name is required"
                                            Font-Bold="true" ForeColor="Red" runat="server"
                                            ValidationGroup="ValidationGroupUpdate" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>Last Name :</td>
                                    <td>
                                        <asp:TextBox ID="txtLastNameUpdate" runat="server" Width="100%" />
                                        <asp:RequiredFieldValidator ID="valLastNameUpdate"
                                            ControlToValidate="txtLastNameUpdate" EnableClientScript="False"
                                            Display="Dynamic" Text="<br />* Last Name is required"
                                            Font-Bold="true" ForeColor="Red" runat="server"
                                            ValidationGroup="ValidationGroupUpdate" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>Address :</td>
                                    <td>
                                        <asp:TextBox ID="txtAddressUpdate" runat="server" Width="100%" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>City :</td>
                                    <td>
                                        <asp:TextBox ID="txtCityUpdate" runat="server" Width="100%" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>Region :</td>
                                    <td>
                                        <asp:TextBox ID="txtRegionUpdate" runat="server" Width="100%" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>Postal Code :</td>
                                    <td>
                                        <asp:TextBox ID="txtPostalCodeUpdate" runat="server" Width="100%" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>Country :</td>
                                    <td>
                                        <asp:TextBox ID="txtCountryUpdate" runat="server" Width="100%" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>Phone :</td>
                                    <td>
                                        <asp:TextBox ID="txtPhoneUpdate" runat="server" Width="100%" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>Email Address :</td>
                                    <td>
                                        <asp:TextBox ID="txtEmailAddressUpdate" runat="server" Width="100%" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="modal-footer">
                            <asp:Label ID="lblResult" Visible="false" runat="server"></asp:Label>
                            <asp:Button ID="btnSave" runat="server" Text="Update"
                                CssClass="btn btn-warning btn-sm" ValidationGroup="ValidationGroupUpdate"
                                OnClick="btnUpdate_Click" />
                            <button class="btn btn-danger btn-sm" data-dismiss="modal"
                                aria-hidden="true">
                                Close</button>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="gv" EventName="RowCommand" />
                        <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>

    <asp:Label ID="lblMessage" runat="server" />

    <!-- Add -->
    <div id="addModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="addModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 id="addModalLabel">Add New Record</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <asp:UpdatePanel ID="addPanel" runat="server">
                    <ContentTemplate>
                        <div class="modal-body">
                            <table class="table table-bordered table-hover">
                                <tr>
                                    <td>First Name :</td>
                                    <td>
                                        <asp:TextBox ID="txtFirstNameAdd" runat="server" Width="100%" />
                                        <asp:RequiredFieldValidator ID="valFirstNameAdd"
                                            ControlToValidate="txtFirstNameAdd" EnableClientScript="False"
                                            Display="Dynamic" Text="<br />* First Name is required"
                                            Font-Bold="true" ForeColor="Red" runat="server"
                                            ValidationGroup="ValidationGroupAdd" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>Last Name :</td>
                                    <td>
                                        <asp:TextBox ID="txtLastNameAdd" runat="server" Width="100%" />
                                        <asp:RequiredFieldValidator ID="valLastNameAdd"
                                            ControlToValidate="txtLastNameAdd" EnableClientScript="False"
                                            Display="Dynamic" Text="<br />* Last Name is required"
                                            Font-Bold="true" ForeColor="Red" runat="server"
                                            ValidationGroup="ValidationGroupAdd" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>Address :</td>
                                    <td>
                                        <asp:TextBox ID="txtAddressAdd" runat="server" Width="100%" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>City :</td>
                                    <td>
                                        <asp:TextBox ID="txtCityAdd" runat="server" Width="100%" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>Region :</td>
                                    <td>
                                        <asp:TextBox ID="txtRegionAdd" runat="server" Width="100%" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>Postal Code :</td>
                                    <td>
                                        <asp:TextBox ID="txtPostalCodeAdd" runat="server" Width="100%" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>Country :</td>
                                    <td>
                                        <asp:TextBox ID="txtCountryAdd" runat="server" Width="100%" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>Phone :</td>
                                    <td>
                                        <asp:TextBox ID="txtPhoneAdd" runat="server" Width="100%" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>Email Address :</td>
                                    <td>
                                        <asp:TextBox ID="txtEmailAddressAdd" runat="server" Width="100%" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="modal-footer">
                            <asp:Button ID="btnAddRecord" runat="server" Text="Add"
                                CssClass="btn btn-success btn-sm" ValidationGroup="ValidationGroupAdd"
                                OnClick="btnAddRecord_Click" />
                            <button class="btn btn-danger btn-sm" data-dismiss="modal"
                                aria-hidden="true">
                                Close</button>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnAddRecord" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>

    <!-- Delete -->
    <div id="deleteModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="delModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 id="delModalLabel">Delete Record</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <asp:UpdatePanel ID="upDel" runat="server">
                    <ContentTemplate>
                        <div class="modal-body">
                            Are you sure you want to delete the record
                            <asp:Label ID="lblFirstNameDelete" runat="server"></asp:Label>?
                            <asp:HiddenField ID="HfDeleteID" runat="server" />
                        </div>

                        <div class="modal-footer">
                            <asp:Button ID="btnDelete" runat="server" Text="Delete"
                                CssClass="btn btn-danger btn-sm" OnClick="btnDelete_Click" />
                            <button class="btn btn-success btn-sm" data-dismiss="modal"
                                aria-hidden="true">
                                Cancel</button>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnDelete" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        function loadTable() {
            $(document).ready(function () {
                $('#<%= gv.ClientID %>').dataTable({
                    "dom": "<'row'<'col-sm-2'l><'col-sm-10 text-right'fB>>" +
                        "<'row'<'col-sm-12'tr>>" +
                        "<'row'<'col-sm-6'i><'col-sm-6'p>>",
                    "lengthMenu": [
                        [10, 25, 50, -1],
                        [10, 25, 50, "All"]
                    ],
                    "order": [
                        [0, "asc"]
                    ],
                    "columnDefs": [
                        { "orderable": false, "targets": 4 },
                        { "className": "dt-head-right", "targets": 4 }
                    ],
                    "buttons": [
                        {
                            text: 'Import',
                            className: 'btn-sm',
                            action: function () {
                                $('#uploadModal').modal('show');
                            }
                        },
                        {
                            extend: 'collection',
                            text: 'Export',
                            autoClose: true,
                            className: 'btn-sm',
                            buttons: [
                                'copyHtml5',
                                'csvHtml5',
                                'excelHtml5',
                                'pdfHtml5',
                                'print'
                            ]
                        }
                    ],
                    stateSave: true,
                    stateSaveCallback: function (settings, data) {
                        localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
                    },
                    stateLoadCallback: function (settings) {
                        return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
                    }
                });
            });
        }
    </script>
</asp:Content>
